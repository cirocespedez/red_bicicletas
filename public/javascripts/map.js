var map = L.map('main_map').setView([-34.600208, -58.646441], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-34.600208, -58.646441]).addTo(map)
L.marker([-34.603356, -58.650137]).addTo(map)
L.marker([-34.592643, -58.636222]).addTo(map)
    
    var circle = L.circle([-34.600208, -58.646441], {
        color: 'orange',
        fillColor: '#ff7300',
        fillOpacity: 0.1,
        radius: 2000
    }).addTo(map);

    $.ajax({
        dataType: "json",
        url: "api/bicicletas",
        success: function(result){
            console.log(result);
            result.bicicletas.forEach(function(bici){
                L.marker(bici.ubicacion, {title: bici.id}).addTo(map);


            });
        }
    })