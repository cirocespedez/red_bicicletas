const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

const base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach((done) => {
        const mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log('We are connected to test database!'); 
            done();  
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, (err, res, body) => {
                let result = JSON.parse(body);
                expect(res.statusCode).toBe(200); 
                expect(res.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            let headers = {'content-type' : 'application/json'};
            let aBici = '{"code": 5, "color": "rojo", "modelo": "montaña", "lat": -25.3, "lng": -58.1}';
            request.post({
                headers,
                url: base_url + '/create',
                body: aBici
            }, (err, res, body) => {
                expect(res.statusCode).toBe(200);
                let bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(ubicacion[0]).toBe(-25.3);
                expect(ubicacion[1]).toBe(-58.1);
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 204', (done) => {
            let headers = {'content-type' : 'application/json'};
            let nbc = '{"id": "test", "color": "test", "modelo": "montaña", "lat": -25.1432424, "lng": -58.9128391}';
            request.post({
                headers,
                url: 'http://localhost:5000/api/bicicletas/update',
                body: nbc
            }, (error, res, body) => {
                expect(res.statusCode).toBe(204);
                expect(Bicicleta.findById("test").color).toBe('test');
                done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            expect(Bicicleta.allBicis.length).toBe(6);
            let headers = {'content-type' : 'application/json'};
            let id = '{"id" : 2}';
            request.delete({
                headers,
                url: 'http://localhost:5000/api/bicicletas/delete',
                body: id
            }, (error, res, body) => {
                expect(res.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(5);
                done();
            });
        });
    });
});